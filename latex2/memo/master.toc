\babel@toc {spanish}{}\relax 
\contentsline {section}{\numberline {1}Introducción}{3}{section.1}%
\contentsline {section}{\numberline {2}Análisis Exploratorio y Tratamiento de Datos}{3}{section.2}%
\contentsline {subsection}{\numberline {2.1}Descripción de la estructura de los datos}{3}{subsection.2.1}%
\contentsline {subsection}{\numberline {2.2}Identificación de datos faltantes}{5}{subsection.2.2}%
\contentsline {subsection}{\numberline {2.3}Identificación de relaciones entre variables}{5}{subsection.2.3}%
\contentsline {subsubsection}{\numberline {2.3.1}Histograma fabricante}{5}{subsubsection.2.3.1}%
\contentsline {subsubsection}{\numberline {2.3.2}Distribución del precio}{6}{subsubsection.2.3.2}%
\contentsline {subsubsection}{\numberline {2.3.3}Variables numéricas}{7}{subsubsection.2.3.3}%
\contentsline {subsubsection}{\numberline {2.3.4}Variables categóricas}{7}{subsubsection.2.3.4}%
\contentsline {subsection}{\numberline {2.4}Predictores Cualitativos}{8}{subsection.2.4}%
\contentsline {section}{\numberline {3}Selección del Modelo}{8}{section.3}%
\contentsline {subsection}{\numberline {3.1}Baseline}{9}{subsection.3.1}%
\contentsline {subsection}{\numberline {3.2}Backward}{11}{subsection.3.2}%
\contentsline {subsection}{\numberline {3.3}Conjunto de Validación}{11}{subsection.3.3}%
\contentsline {subsection}{\numberline {3.4}Validación Cruzada de $k$-grupos}{12}{subsection.3.4}%
\contentsline {subsection}{\numberline {3.5}Forward}{13}{subsection.3.5}%
\contentsline {subsection}{\numberline {3.6}Conjunto de Validación}{13}{subsection.3.6}%
\contentsline {subsection}{\numberline {3.7}Validación Cruzada de $k$-grupos}{14}{subsection.3.7}%
\contentsline {subsection}{\numberline {3.8}Best Subset}{15}{subsection.3.8}%
\contentsline {subsection}{\numberline {3.9}Conjunto de Validación}{15}{subsection.3.9}%
\contentsline {subsection}{\numberline {3.10}Validación Cruzada de $k$-grupos}{16}{subsection.3.10}%
\contentsline {subsection}{\numberline {3.11}Resultado}{17}{subsection.3.11}%
\contentsline {section}{\numberline {4}Diagnóstico del Modelo}{18}{section.4}%
\contentsline {subsection}{\numberline {4.1}No normalidad y varianzas no constantes}{18}{subsection.4.1}%
\contentsline {subsection}{\numberline {4.2}Linealidad}{20}{subsection.4.2}%
\contentsline {subsection}{\numberline {4.3}Autocorrelación}{21}{subsection.4.3}%
\contentsline {subsection}{\numberline {4.4}Colinealidad}{21}{subsection.4.4}%
\contentsline {subsection}{\numberline {4.5}Outliers, High Leverage Values y Observaciones Influyentes}{21}{subsection.4.5}%
\contentsline {subsection}{\numberline {4.6}Outliers}{21}{subsection.4.6}%
\contentsline {subsection}{\numberline {4.7}High Leverage Values}{22}{subsection.4.7}%
\contentsline {subsection}{\numberline {4.8}Observaciones Influyentes}{23}{subsection.4.8}%
\contentsline {section}{\numberline {5}Evaluación del Modelo}{23}{section.5}%
\contentsline {subsection}{\numberline {5.1}Reevaluación}{23}{subsection.5.1}%
\contentsline {subsection}{\numberline {5.2}Nuevas Observaciones}{23}{subsection.5.2}%
\contentsline {section}{\numberline {6}Conclusión}{24}{section.6}%
