\section{Diagnóstico del Modelo}

La regresión lineal asume ciertas hipótesis sobre los datos. En esta sección estudiaremos si el modelo que hemos seleccionado cumple estas hipótesis. En caso de que haya alguna hipótesis que no se cumpla trataremos de minimizar los problemas que puede causar. Estas hipótesis son

\begin{enumerate}
  \item Linealidad entre la variable respuesta y la predictora
  \item Normalidad Residuos
  \item Homogeneidad Varianza
  \item Correlación de los Residuos
  \item Colinealidad
  \item Outliers
  \item Puntos de Alto Leverage
  \item Valores influyentes
\end{enumerate}

\subsection{No normalidad y varianzas no constantes}

En la 'figura' se disponen dos gráficos. El gráfico de la izquierda \textit{Scale-Location Plot}, nos muestra la variación de los residuos en el rango de los predictores. Como la recta de ajuste no es una recta horizontal si no más bien tiene tendencia logarítmica, deducimos que la varianza no es constante. En el gráfico de la derecha \textit{Normal QQ Plot}, dado que el patrón de los datos no sigue una línea recta deducimos que los residuos no son normales.   

\begin{figure}[H]
    \centering
    \fbox{\includegraphics[scale=0.5]{diag2}}
    \caption{Gráficas Diagnóstico Normalidad Residudos y Homogeneidad Varianza}
\end{figure}

\noindent Para minimizar los efectos de la no normalidad de los residuos y la no homogeneidad de sus varianzas aplicamos una transformación a la variable respuesta \textit{price} mediante la función \textit{BoxCos}. En primer lugar, determinamos el valor $\lambda$ correspondiente

\begin{lstlisting}[language=R, caption=BoxCox]
> box_cox <- boxcox(formula, data=df2)
> (lambda <- box_cox$x[which.max(box_cox$y)])
[1] 0.1010101
\end{lstlisting}

\begin{figure}[H]
    \centering
    \fbox{\includegraphics[scale=0.4]{boxCox}}
    \caption{Gráfica BoxCox}
\end{figure}

Obtenemo $\lambda = 0.1010101 \neq 0$ correspondiente al intervalo de confianza de la 'figura'. Por tanto, aplicamos la transformación
\[ 
  Y' = \frac{Y^{\lambda -1}}{\lambda} .
\] 

Comprobando de nuevo la normalidad de los resiuduos y su varianza constante tenemos que este modelo transformado se ajusta mejor a los datos.

\begin{figure}[H]
    \centering
    \fbox{\includegraphics[scale=0.5]{daig3}}
    \caption{Gráficas Diagnóstico Normalidad Residudos y Homogeneidad Varianza 2}
\end{figure}

\subsection{Linealidad}

En la 'figura' se presenta un gráfico de los residuos de los valores ajustados. La recta de ajuste es similar a una rezta horizontal, no presenta ningún patrón. Por tanto, la relación entre las variables predictoras y la respuesta es lineal.

\begin{figure}[H]
    \centering
    \fbox{\includegraphics[scale=0.4]{linealidad}}
    \caption{Gráfica Linealidad}
\end{figure}

\subsection{Autocorrelación}

A partir del test de Durbin-Watson

\[ 
  DW = \frac{\sum_{i=1}^{n}(\hat{ \epsilon }_{i} - \hat{ \epsilon }_{i-1})^{2}}{\sum_{i=1}^{n}\hat{ \epsilon }_{i}} 
\] 

\noindent obtenemos un $p\text{-valor} = 0.153 > 0.05$ y $DW = 1.822691$ cercano a $2$. Por tanto $\rho = 0$ y aceptamos la hipótesis nula, es decir, no hay autocorrelación.

\begin{lstlisting}[language=R, caption=Autocorrelación Durbin-Watson]
> durbinWatsonTest(modelo)
 lag Autocorrelation D-W Statistic p-value
   1       0.3243857      1.822691   0.153
 Alternative hypothesis: rho == 0
\end{lstlisting}

\subsection{Colinealidad}

Comprobamos la colinealidad, es decir, si alguna de las variables es combinación lineal de las otras usando el factor de inflación de varianza
\[ 
  \vif = \frac{1}{1 - R^2_{j}} 
\] 
\noindent donde $j$ es el índice de las variables del modelo. 

\begin{lstlisting}[language=R, caption=Colinealidad VIF]
> ols_vif_tol(modelo2)
            Variables  Tolerance       VIF
1       enginetypeohc 0.36085831  2.771171
2      enginetypeohcf 0.54856721  1.822931
3      enginetypeohcv 0.35861966  2.788470
4         fueltypegas 0.59920944  1.668866
5    carbodyhatchback 0.17269678  5.790496
6        carbodysedan 0.16771649  5.962443
7        carbodywagon 0.30996365  3.226185
8  cylindernumberfive 0.10965997  6.119098
9  cylindernumberfour 0.03345039  5.895018
10  cylindernumbersix 0.08126950  8.304739
11  cylindernumbertwo 0.14797438  6.757927
12      drivewheelrwd 0.43195319  2.315066
13           carwidth 0.23114781  4.326236
14         enginesize 0.09568509  3.450949
15         horsepower 0.19297707  5.181963
\end{lstlisting}

\noindent En la 'figura' tenemos los factores de inflación de la varianza asociados a cada variable del modelo. Dado que ninguno es mayor que $10$ concluimos que las variables del modelo no representan ningún indicio de colinealidad grave.

\subsection{Outliers, High Leverage Values y Observaciones Influyentes}
\subsection{Outliers}

En la 'figura' se presenta un gráfico de los residuos estandarizados respecto de su leverage. Las observaciones $129, 137$ y $171$ destacan por encima de las demás pero aplicando estadístico de Bonferroni obtemos $\rho = 0.69654 > 0.05$ por tanto, concluimos que no hay outliers.

\begin{figure}[H]
    \centering
    \fbox{\includegraphics[scale=0.5]{outliers}}
    \caption{Outliers}
\end{figure}

\begin{lstlisting}[language=R, caption=Colinealidad VIF]
> outlierTest(modelo2)
No Studentized residuals with Bonferroni p < 0.05
Largest |rstudent|:
     rstudent unadjusted p-value Bonferroni p
171 -2.935182          0.0038062      0.69654
\end{lstlisting}

\subsection{High Leverage Values}

Para obtener los valores de alto leverage calculamos $\alpha$ la media de los \textit{hatvalues} y observamos los puntos que superen el doble de este valor. En la 'figura' se representa el \textit{hatvalue} de cada observación. De entre estas observaciones las que superan $\alpha$ son las que figuran en el 'listing'

\begin{lstlisting}[language=R, caption=High Leverage]
> hats <- as.data.frame(hatvalues(modelo2))
> r <- 2*length(coef(modelo2))/nrow(df2)
> which(hats>r)
 [1]   3   6   7  43  49  50  51  52  61  62  64  65  66  67  93  94
[17]  95  97 114 115 116 181 182
\end{lstlisting}

\begin{figure}[H]
    \centering
    \fbox{\includegraphics[scale=0.5]{hats}}
    \caption{Hatvalues}
\end{figure}

\subsection{Observaciones Influyentes}

Dado que no hemos obtenido ningún outlier comprobamos si las observaciones con alto leverage son influyentes. Pero ninguna es influyente en el modelo.

\begin{figure}[H]
    \centering
    \fbox{\includegraphics[scale=0.5]{cook}}
    \caption{InfluencePlot}
\end{figure}
