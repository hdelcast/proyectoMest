library(car)
library(lmtest)
library(fastDummies)
library(stringr)
library(dplyr)
library(MASS)
library(leaps)
library(MASS)
library(tidyverse)
library(caret)
library(mltools)

# importación datos


data0 <- read.csv("./CarPrice_Assignment.csv", header = TRUE)
head(data0)
summary(data0)
w <- c(17,150,130,168,169,49,50)
data0 <- data0[-w,]

set.seed(1)
sample0 <- sample(c(TRUE, FALSE), nrow(data0), replace=TRUE, prob=c(0.9,0.1))
data  <- data0[sample0, ]
prueba   <- data0[!sample0, ]


### Bestsubset

var <- c("price", "enginetype", "fueltype", "carbody", "aspiration",
         "cylindernumber", "drivewheel", "curbweight", "carlength",
         "carwidth", "enginesize", "boreratio", "horsepower", "wheelbase",
         "citympg", "highwaympg")

df1 <- data
df1 <- df1 %>% dplyr::select(all_of(var))
df1[sapply(df1, is.character)] <- lapply(df1[sapply(df1, is.character)], as.factor)
prueba <- prueba  %>% dplyr::select(all_of(var))
prueba[sapply(prueba, is.character)] <- lapply(prueba[sapply(prueba, is.character)], as.factor)

###### Bestsubset Aproximación al conjunto de validación 

# split 
set.seed(1)
sample <- sample(c(TRUE, FALSE), nrow(df1), replace=TRUE, prob=c(0.7,0.3))
train  <- df1[sample, ]
test   <- df1[!sample, ]

# Subconjuntos
bestModel <- regsubsets(price~., data=train, nbest=1, nvmax=30, method="exhaustive")

test.mat <- model.matrix(price ~ ., data = test)

k <- length(bestModel) - 4
val.errors <- rep(NA, k)
q <- matrix(NA, nrow = k, ncol = 3)
colnames(q) <- c('R2','RMSE', 'MAE')
for ( i in 1:k){
  coefi <- coef(bestModel , id=i)
  pred <- test.mat[ ,names(coefi)] %*% coefi
  val.errors[i] <- mean((test$price - pred)^2)
  q[i, 1] <- R2(pred, test$price)
  q[i, 2] <- RMSE(pred, test$price)
  q[i, 3] <- MAE(pred, test$price)
}

## Valerror
par(mfrow = c(2, 2))
## MSE
val.errors
min1 = which.min(val.errors)
val.errors[min1]
plot(val.errors, type = 'b', ylab='MSE', xlab='número variables')
points(x=min1,y=val.errors[min1], col='red', pch=3)

## R2
plot(q[,1], type = 'b', ylab='R2', xlab='número variables')
min11 = which.max(q[,1])
min11
q[,1][min11]
points(x=min11,y=q[,1][min11], col='red', pch=3)

## RSME
plot(q[,2], type = 'b', ylab='RMSE', xlab='número variables')
min12 = which.min(q[,2])
min12
q[,2][min12]
points(x=min12,y=q[,2][min12], col='red', pch=3)

## MAE
plot(q[,3], type = 'b', ylab='MAE', xlab='número variables')
min13 = which.min(q[,3])
min13
q[,3][min13]
points(x=min13,y=q[,3][min13], col='red', pch=3)

## coeficientes
bestModel1 <- regsubsets(price~., data=df1, nbest=1, nvmax=30, method="exhaustive")
coef(bestModel1, min1)

## fórmula

#labels(coef(bestModel1, min11))
p <- summary(bestModel1)$which[15,-1]
predictors <- names(which(p == TRUE))
predictors <- paste(predictors, collapse = " + ")
predictors
formula <- as.formula(paste0('price', "~", predictors))

matrix <- model.matrix(~ ., data=df1)
df2 <- data.frame(matrix[1:nrow(df1),])
df2 <- df2[-1]
#all <- lm(price ~ ., data=df2)
modelo <- lm(formula, data=df2)
summary(modelo)


## Diagnóstico

par(mfrow=c(1,2)) 
## Linealidad Datos (Los datos son lineales)
plot(modelo, 1)

## Homogeneidad Varianza (La varianza no es constante)
plot(modelo, 3)

## Normalidad Residuos (Los residuos no son normales)
plot(modelo, 2)
shapiro.test(resid(modelo))

## Correlación
ols_test_correlation(modelo)
durbinWatsonTest(modelo)

## Colinealidad
ols_vif_tol(modelo2)

## outliers
plot(modelo, 5)
outlierTest(modelo2)

## highleverage
hats <- as.data.frame(hatvalues(modelo2))
r <- 2*length(coef(modelo2))/nrow(df2)
which(hats>r)
barplot(hatvalues(modelo2), col = "aquamarine3")
influence.measures(modelo2)
influenceIndexPlot(modelo2)
influencePlot(modelo2)

## obs influyentes (no hay?)
plot(modelo, 4)
4/(length(nrow(df2) - coef(modelo)) - 1)
plot(modelo, 5)


## Transformación (aplicamos boxcox para determinar transformación)
## Recalcular modelo
box_cox <- boxcox(formula, data=df2)
(lambda <- box_cox$x[which.max(box_cox$y)])

df3 <- df2
df3$price <- (df3$price^lambda - 1)/lambda

modelo2 <- lm(formula, data=df3)
summary(modelo2)



########
## Ahora si que sale todo bien

par(mfrow=c(1,2)) 
plot(modelo2, 3)
plot(modelo2, 2)

## Linealidad de los datos
plot(modelo2, 1)

## Varianza constante residuos
plot(modelo2, 3)

## Normalidad Residuos
shapiro.test(resid(modelo2))
plot(modelo2, 2)

## coor
ols_test_correlation(modelo2)
durbinWatsonTest(modelo)


# obs influyentes
plot(modelo, 4)
plot(modelo2, 5)

# split 
set.seed(1)
sample <- sample(c(TRUE, FALSE), nrow(df3), replace=TRUE, prob=c(0.7,0.3))
train  <- df3[sample, ]
test   <- df3[!sample, ]


## reevaluación
pp <- modelo2 %>% predict(test)
mse <- mean((test$price - pp)^2)
data.frame( MSE = mse,
            R2 = R2(pp, test$price),
            RMSE = RMSE(pp, test$price),
            MAE = MAE(pp, test$price))


## Nuevas observaciones
prueba$price <- (prueba$price^lambda - 1)/lambda

pp <- modelo2 %>% predict(prueba2)
mse <- mean((prueba$price - pp)^2)
data.frame( MSE = mse,
            R2 = R2(pp, test$prueba),
            RMSE = RMSE(pp, test$prueba),
            MAE = MAE(pp, test$prueba))
